using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace PastebinAutomationTests
{
    public class PastebinHomePage
    {
        private readonly IWebDriver driver;

        public PastebinHomePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement NewPasteField => driver.FindElement(By.Id("postform-text"));
        private IWebElement SyntaxHighlightDropdown => driver.FindElement(By.Id("select2-postform-format-container"));
        private IWebElement PasteExpirationDropdown => driver.FindElement(By.Id("select2-postform-expiration-container"));
        private IWebElement PasteNameField => driver.FindElement(By.Id("postform-name"));
        private IWebElement CreateNewPasteButton => driver.FindElement(By.XPath("//button[text()='Create New Paste']"));

        public void EnterPasteCode(string code)
        {
            NewPasteField.SendKeys(code);
        }

        public void SelectSyntaxHighlighting(string syntax)
        {
            SyntaxHighlightDropdown.Click();
            driver.FindElement(By.XPath($"//li[text()='{syntax}']")).Click();
        }

        public void SelectPasteExpiration(string expiration)
        {
            PasteExpirationDropdown.Click();
            driver.FindElement(By.XPath($"//li[text()='{expiration}']")).Click();
        }

        public void EnterPasteName(string pasteName)
        {
            PasteNameField.SendKeys(pasteName);
        }

        public void CreatePaste()
        {
            CreateNewPasteButton.Click();
        }
    }

    public class PastebinPastePage
    {
        private readonly IWebDriver driver;

        public PastebinPastePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        private IWebElement PageTitle => driver.FindElement(By.TagName("h1"));
        private IWebElement CodeContent => driver.FindElement(By.XPath("//ol[@class='bash']"));

        public string GetPageTitle()
        {
            return PageTitle.Text;
        }

        public string GetCodeContent()
        {
            return CodeContent.Text;
        }
    }

    [TestFixture]
    public class PastebinTests
    {
        private IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
        }

        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }

        [Test]
        public void CreateAndVerifyPaste()
        {
            driver.Navigate().GoToUrl("https://pastebin.com/");

            var homePage = new PastebinHomePage(driver);

            var code = @"
git config --global user.name  ""New Sheriff in Town""
git reset $(git commit-tree HEAD^{tree} -m ""Legacy code"")
git push origin master --force
";
            var syntax = "Bash";
            var expiration = "10 Minutes";
            var pasteName = "how to gain dominance among developers";

            homePage.EnterPasteCode(code);
            homePage.SelectSyntaxHighlighting(syntax);
            homePage.SelectPasteExpiration(expiration);
            homePage.EnterPasteName(pasteName);
            homePage.CreatePaste();

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElement(By.TagName("h1")));

            var pastePage = new PastebinPastePage(driver);

            Assert.AreEqual(pasteName, pastePage.GetPageTitle(), "Page title does not match paste name.");
            Assert.IsTrue(pastePage.GetCodeContent().Contains("git config"), "Code content does not match.");
        }
    }
}
